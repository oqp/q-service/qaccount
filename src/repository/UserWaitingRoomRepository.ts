import { Types } from 'mongoose'
import IUserWaitingRoomRepository from './interface/IUserWaitingRoomRepository'
import UserWaitingRoom, { UserWaitingRoomModel } from '../model/UserWaitingRoom'

export default class UserWaitingRoomRepository implements IUserWaitingRoomRepository {
  async findByUserId(userId: string): Promise<UserWaitingRoom[]> {
    const userIdObjectId = new Types.ObjectId(userId)
    const userWaitingRoom = await UserWaitingRoomModel.find({ userId: userIdObjectId })
    const userWaitingRoomArray = new Array<UserWaitingRoom>(0)
    userWaitingRoom.forEach((mongooseEntity) => {
      const entity = new UserWaitingRoom()
      entity.parseFromMongoose(mongooseEntity)
      userWaitingRoomArray.push(entity)
    })
    return userWaitingRoomArray
  }

  async findByWaitingRoomId(waitingRoomId: string): Promise<UserWaitingRoom[]> {
    const waitingRoomObjectId = new Types.ObjectId(waitingRoomId)
    const userWaitingRoom = await UserWaitingRoomModel.find({ waitingRoomId: waitingRoomObjectId })
    const userWaitingRoomArray = new Array<UserWaitingRoom>(0)
    userWaitingRoom.forEach((mongooseEntity) => {
      const entity = new UserWaitingRoom()
      entity.parseFromMongoose(mongooseEntity)
      userWaitingRoomArray.push(entity)
    })
    return userWaitingRoomArray
  }

  async save(userWaitingRoom: UserWaitingRoom): Promise<UserWaitingRoom> {
    const userWaitingRoomMongoose = await UserWaitingRoomModel.create(userWaitingRoom)
    const createdUserWaitingRoom = new UserWaitingRoom()
    createdUserWaitingRoom.parseFromMongoose(userWaitingRoomMongoose)
    return createdUserWaitingRoom
  }
}
