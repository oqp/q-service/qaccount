import { Types } from 'mongoose'
import IEventRepository from './interface/IEventRepository'
import Event, { EventModel } from '../model/Event'

export default class EventRepository implements IEventRepository {
  async save(event: Event): Promise<Event> {
    const mongooseEvent = await EventModel.create(event)
    const returnEvent = new Event()
    returnEvent.parseFromMongoose(mongooseEvent)
    return returnEvent
  }

  async findById(eventId: string): Promise<Event | null> {
    const eventMongoose = await EventModel.findById(eventId)
    if (eventMongoose === null) {
      return null
    }
    const event = new Event()
    event.parseFromMongoose(eventMongoose)
    return event
  }

  async findOne(findObject: object): Promise<Event | null> {
    const eventMongoose = await EventModel.findOne(findObject)
    if (eventMongoose === null) {
      return null
    }
    const event = new Event()
    event.parseFromMongoose(eventMongoose)
    return event
  }

  async findByWaitingRoomId(waitingRoomId: string): Promise<Event[]> {
    const eventMongoose = await EventModel.find({ waitingRoomId: Types.ObjectId(waitingRoomId) })
    const event: Event[] = new Array<Event>()
    eventMongoose.forEach((eventMongooseModel) => {
      const eventTemp = new Event()
      eventTemp.parseFromMongoose(eventMongooseModel)
      event.push(eventTemp)
    })
    return event
  }

  async update(event: Event): Promise<Event | null> {
    const eventId = event.id
    delete event.id
    const eventMongoose = await EventModel.findByIdAndUpdate(eventId, event, { new: true })
    if (eventMongoose == null) {
      return null
    }
    const returnEvent = new Event()
    returnEvent.parseFromMongoose(eventMongoose)
    return returnEvent
  }

  async delete(event: Event): Promise<Event | null> {
    const eventMongoose = await EventModel.deleteOne({ _id: event.id })
    if (eventMongoose.deletedCount === 0) {
      return null
    }
    return event
  }
}
