import IWaitingRoomRepository from './interface/IWaitingRoomRepository'
import WaitingRoom, { WaitingRoomModel } from '../model/WaitingRoom'

export default class WaitingRoomRepository implements IWaitingRoomRepository {
  async findById(waitingRoomId: string): Promise<WaitingRoom | null> {
    const waitingRoom = await WaitingRoomModel.findOne({ _id: waitingRoomId })
    if (waitingRoom == null) {
      return null
    }
    const returnWaitingRoom = new WaitingRoom()
    returnWaitingRoom.parseFromMongoose(waitingRoom)
    return returnWaitingRoom
  }

  async save(waitingRoom: WaitingRoom): Promise<WaitingRoom> {
    const mongooseWaitingRoom = await WaitingRoomModel.create(waitingRoom)
    const newWaitingRoom = new WaitingRoom()
    newWaitingRoom.parseFromMongoose(mongooseWaitingRoom)
    return newWaitingRoom
  }

  async update(waitingRoomId: string, waitingRoom: WaitingRoom): Promise<WaitingRoom | null> {
    delete waitingRoom.id
    const mongooseWaitingRoom = await WaitingRoomModel.findByIdAndUpdate(waitingRoomId, waitingRoom, { new: true })
    if (mongooseWaitingRoom === null) {
      return null
    }
    const updatedWaitingRoom = new WaitingRoom()
    updatedWaitingRoom.parseFromMongoose(mongooseWaitingRoom)
    return updatedWaitingRoom
  }
  
  async delete(waitingRoom: WaitingRoom): Promise<WaitingRoom | null> {
    const waitingRoomMongoose = await WaitingRoomModel.deleteOne({ _id: waitingRoom.id })
    if (waitingRoomMongoose.deletedCount === 0) {
      return null
    }
    return waitingRoom
  }
}
