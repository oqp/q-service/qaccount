import WaitingRoom from '../../model/WaitingRoom'

export default interface IWaitingRoomRepository {
  findById(waitingRoomId: string): Promise<WaitingRoom | null>;
  save(waitingRoom: WaitingRoom): Promise<WaitingRoom>;
}
