import Event from '../../model/Event'

export default interface IEventRepository {
  save(event: Event): Promise<Event>;
  findById(eventId: string): Promise<Event | null>;
  findByWaitingRoomId(waitingRoomId: string): Promise<Event[]>;
  update(event: Event): Promise<Event | null> ;
}
