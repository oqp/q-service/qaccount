import express from 'express'
import HttpResponse from '../http/response/HTTPResponse'
import UserService from '../service/UserService'
import UserRegisterByEmailRequestEntity from '../http/request/UserRegisterByEmailRequestEntity'
import User from '../model/User'
import UserLoginByEmailRequestEntity from '../http/request/UserLoginByEmailRequestEntity'
import IRequestWithUser from '../http/interface/IRequestWithUser'
import RequestException from '../exception/RequestException'

const router = express.Router()

router.post('/register/email', async (req, res) => {
  const userService = new UserService()
  try {
    const request = new UserRegisterByEmailRequestEntity()
    request.parseFromBodyRequest(req.body)
    const createdUser: User = await userService.registerByEmail(request)
    res.status(200).json(new HttpResponse(2001, 'created', createdUser))
  } catch (error) {
    if (error instanceof RequestException) {
      res.status(error.httpStatusCode).json(new HttpResponse(error.code, error.toString(), null))
    } else {
      res.status(500).json(new HttpResponse(5000, error.toString(), null))
    }
  }
})

router.post('/login/email', async (req, res) => {
  const userService = new UserService()
  try {
    const request = new UserLoginByEmailRequestEntity()
    request.parseFromBodyRequest(req.body)
    const token: string = await userService.loginByEmail(request)
    res.status(200).json(new HttpResponse(2002, 'login success', { accessToken: token }))
  } catch (error) {
    if (error instanceof RequestException) {
      res.status(error.httpStatusCode).json(new HttpResponse(error.code, error.toString(), null))
    } else {
      res.status(500).json(new HttpResponse(5000, error.toString(), null))
    }
  }
})

router.get('/me', async (req, res) => {
  const reqWithUser = req as IRequestWithUser
  res.status(200).json(new HttpResponse(2000, '', reqWithUser.user))
})

router.get('/refreshToken', async (req, res) => {
  const userService = new UserService()
  const reqWithUser = req as IRequestWithUser
  const token = await userService.refreshToken(reqWithUser.user)
  res.status(200).json(new HttpResponse(2000, 'refresh token success', { accessToken: token }))
})

export = router
