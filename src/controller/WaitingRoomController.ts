import express from 'express'
import WaitingRoomService from '../service/WaitingRoomService'
import CreateWaitingRoomRequestEntity from '../http/request/CreateWaitingRoomRequestEntity'
import IRequestAsUser from '../http/interface/IRequestWithUser'
import IRequestWithEvent from '../http/interface/IRequestWithEvent'
import HTTPResponse from '../http/response/HTTPResponse'
import RequestException from '../exception/RequestException'
import UserCreateNewEventRequestEntity from '../http/request/UserCreateNewEventRequestEntity'
import EventService from '../service/EventService'
import UserWaitingRoomValidator from '../middleware/UserWaitingRoomValidator'
import UserEventValidator from '../middleware/UserEventValidator'
import PrometheusService from '../service/PrometheusService'
import DuplicateSubdomain from '../exception/DuplicateSubdomain'
import IRequestWithWaitingRoom from '../http/interface/IRequestWithWaitingRoom'

const router = express.Router()

router.use('/:waiting_room_id', UserWaitingRoomValidator)
router.use('/:waiting_room_id/event/:event_id', UserEventValidator)

router.get('/', async (req, res) => {
  const waitingRoomService = new WaitingRoomService()
  const reqWithUser = req as IRequestAsUser

  try {
    const waitingRooms = await waitingRoomService.findWaitingRoomByUser(reqWithUser.user)
    const response = new HTTPResponse(2000, 'OK', waitingRooms)
    res.status(200).json(response)
  } catch (error) {
    if (error instanceof RequestException) {
      const requestException = error as RequestException
      const response = new HTTPResponse(requestException.code, requestException.message, null)
      res.status(requestException.httpStatusCode).json(response)
    } else {
      const response = new HTTPResponse(5000, error.toString(), null)
      res.status(500).json(response)
    }
  }
})

router.post('/', async (req, res) => {
  const waitingRoomService = new WaitingRoomService()
  const reqWithUser = req as IRequestAsUser

  const request = new CreateWaitingRoomRequestEntity()
  request.parseFromBodyRequest(req.body)

  try {
    const waitingRoom = await waitingRoomService.createWaitingRoom(reqWithUser.user, request)
    const response = new HTTPResponse(2001, 'Created', waitingRoom)
    res.status(201).json(response)
  } catch (error) {
    if (error instanceof RequestException) {
      const requestException = error as RequestException
      const response = new HTTPResponse(requestException.code, requestException.message, null)
      res.status(requestException.httpStatusCode).json(response)
    } else {
      const response = new HTTPResponse(5000, error.toString(), null)
      res.status(500).json(response)
    }
  }
})

router.put('/:waiting_room_id', async (req, res) => {
  const { waitingRoom } = req as IRequestWithWaitingRoom
  const waitingRoomService = new WaitingRoomService()
  const waitingRoomRequest = new CreateWaitingRoomRequestEntity()
  waitingRoomRequest.parseFromBodyRequest(req.body)
  const waitingRoomId = waitingRoom.id
  try {
    const waitingRoom = await waitingRoomService.editWaitingRoom(waitingRoomId, waitingRoomRequest)
    const response = new HTTPResponse(2000, 'Successfully updated waiting room', waitingRoom)
    res.status(200).json(response)
  } catch (error) {
    if (error instanceof RequestException) {
      const requestException = error as RequestException
      const response = new HTTPResponse(requestException.code, requestException.message, null)
      res.status(requestException.httpStatusCode).json(response)
    } else {
      const response = new HTTPResponse(5000, error.toString(), null)
      res.status(500).json(response)
    }
  }
})

router.get('/:waiting_room_id', async (req, res) => {
  const { waitingRoom } = req as IRequestWithWaitingRoom
  res.status(200).json(new HTTPResponse(2000, 'OK', waitingRoom))
})

router.post('/:waiting_room_id/event', async (req, res) => {
  const eventService = new EventService()
  const { waitingRoom } = req as IRequestWithWaitingRoom

  const requestBody = new UserCreateNewEventRequestEntity()
  requestBody.parseFromBodyRequest(req.body)
  try {
    const event = await eventService.addEvent(requestBody, waitingRoom)
    res.status(201).json(new HTTPResponse(2001, 'Created', event))
  } catch (err) {
    if (err instanceof RequestException) {
      const reqErr = err as RequestException
      res.status(reqErr.httpStatusCode).json(reqErr.getHTTPResponse())
    } else {
      res.status(500).json(new HTTPResponse(5000, err.toString(), null))
    }
  }
})

router.get('/:waiting_room_id/event', async (req, res) => {
  const eventService = new EventService()
  const { waitingRoom } = req as IRequestWithWaitingRoom

  const events = await eventService.findByWaitingRoomId(waitingRoom.id)
  res.status(200).json(new HTTPResponse(2000, 'OK', events))
})

router.get('/:waiting_room_id/event/:event_id', async (req, res) => {
  const { event } = req as IRequestWithEvent
  res.status(200).json(new HTTPResponse(2000, 'OK', event))
})

router.get('/:waiting_room_id/event/:event_id/metrics', async (req, res) => {
  const prometheusService = new PrometheusService()
  const reqWithEvent = req as IRequestWithEvent
  try {
    const currentQueueNumber = await prometheusService.getCurrentQueueNumber(reqWithEvent.event)
    const maxSessionTime = await prometheusService.getMaxSessionTime(reqWithEvent.event)
    const minSessionTime = await prometheusService.getMinSessionTime(reqWithEvent.event)
    const avgSessionTime = await prometheusService.getAvgSessionTime(reqWithEvent.event)
    const completedAudience = await prometheusService.getCompletedAudience(reqWithEvent.event)
    const sessionTimedoutAudience = await prometheusService.getSessionTimedoutAudience(reqWithEvent.event)
    const waitingAudience = await prometheusService.getWaitingAudience(reqWithEvent.event)
    const arrivalTimedoutAudience = await prometheusService.getArrivalTimedoutAudience(reqWithEvent.event)
    const servingAudience = await prometheusService.getServingAudience(reqWithEvent.event)
    const data = {
      currentQueueNumber: {
        labels: ['currentQueueNumber'],
        values: [currentQueueNumber],
      },
      maxSessionTime: {
        labels: ['maxSessionTime'],
        values: [maxSessionTime],
      },
      minSessionTime: {
        labels: ['minSessionTime'],
        values: [minSessionTime],
      },
      avgSessionTime: {
        labels: ['avgSessionTime'],
        values: [avgSessionTime],
      },
      completedAudience: {
        labels: ['completedAudience'],
        values: [completedAudience],
      },
      sessionTimedoutAudience: {
        labels: ['sessionTimedoutAudience'],
        values: [sessionTimedoutAudience],
      },
      waitingAudience: {
        labels: ['waitingAudience'],
        values: [waitingAudience],
      },
      arrivalTimedoutAudience: {
        labels: ['arrivalTimedoutAudience'],
        values: [arrivalTimedoutAudience],
      },
      servingAudience: {
        labels: ['servingAudience'],
        values: [servingAudience],
      },
    }
    const response = new HTTPResponse(2000, 'OK', data)
    res.status(200).json(response)
  } catch (err) {
    const response = new HTTPResponse(5000, err.toString(), err)
    res.status(500).json(response)
  }
})

router.post('/:waiting_room_id/event/:event_id/integrateToken', async (req, res) => {
  const eventService = new EventService()
  const { event } = req as IRequestWithEvent
  const integrateToken = await eventService.generateIntegrateToken(event.id)
  const responseObj = {
    integrateToken,
  }
  res.status(201).json(new HTTPResponse(2001, 'Token for integrate website successfully generated.', responseObj))
})

router.post('/:waiting_room_id/event/:event_id/isActive', async (req, res) => {
  const eventService = new EventService()
  const { event } = req as IRequestWithEvent
  const { isActive } = req.body
  if (isActive === undefined || isActive === null) {
    res.status(400).json(new HTTPResponse(4000, 'isActive not specify', null))
  }
  try {
    const updatedEvent = await eventService.setEventIsActiveStatus(event.id, isActive)
    res.status(200).json(new HTTPResponse(2000, 'OK', updatedEvent))
  } catch (error) {
    if (error instanceof RequestException) {
      res.status(error.httpStatusCode).json(error)
    } else {
      res.status(500).json(new HTTPResponse(5000, 'Failed setting event status', null))
    }
  }
})

router.put('/:waiting_room_id/event/:event_id', async (req, res) => {
  const { event } = req as IRequestWithEvent
  const eventService = new EventService()
  const requestBody = new UserCreateNewEventRequestEntity()
  requestBody.parseFromBodyRequest(req.body)

  try {
    const newEvent = await eventService.editEvent(event.id, requestBody)
    res.status(200).json(new HTTPResponse(2000, 'Successfully updated event.', newEvent))
  } catch (error) {
    if (error instanceof DuplicateSubdomain) {
      res.status(error.httpStatusCode).json(new HTTPResponse(error.code, error.message, {
        subdomain: requestBody.subdomain,
      }))
    } else {
      const response = new RequestException(5000, 'Internal Server Error', 500)
      res.status(response.httpStatusCode).json(new HTTPResponse(response.code, response.message, null))
    }
  }
})

router.delete('/:waiting_room_id/event/:event_id', async (req, res) => {
  const eventService = new EventService()
  const { event } = req as IRequestWithEvent
  try {
    const deletedEvent = await eventService.deleteEvent(event)
    if (deletedEvent === null) {
      res.status(400).json(new HTTPResponse(4000, 'event not found', null))
    } else {
      res.status(200).json(new HTTPResponse(2000, 'OK', null))
    }
  } catch (error) {
    if (error instanceof RequestException) {
      const requestException = error as RequestException
      const response = new HTTPResponse(requestException.code, requestException.message, null)
      res.status(requestException.httpStatusCode).json(response)
    } else {
      const response = new HTTPResponse(5000, error.toString(), null)
      res.status(500).json(response)
    }
  }
})

router.delete('/:waiting_room_id', async (req, res) => {
  const { waitingRoom } = req as IRequestWithWaitingRoom
  const eventService = new EventService()
  const waitingRoomService = new WaitingRoomService()
  const events = await eventService.findByWaitingRoomId(waitingRoom.id)
  if (events.length !== 0) {
    const response = new HTTPResponse(4000, 'You need to delete event first', events.map((event) => {
      return {
        id: event.id,
        name: event.name,
      }
    }))
    res.status(400).json(response)
  } else {
    try {
      const deletedWaitingRoom = await waitingRoomService.deleteWaitingRoom(waitingRoom)
      if (deletedWaitingRoom === null) {
        res.status(400).json(new HTTPResponse(4000, 'WaitingRoom not found', null))
      } else {
        res.status(200).json(new HTTPResponse(2000, 'OK', null))
      }
    } catch (error) {
      if (error instanceof RequestException) {
        const requestException = error as RequestException
        const response = new HTTPResponse(requestException.code, requestException.message, null)
        res.status(requestException.httpStatusCode).json(response)
      } else {
        const response = new HTTPResponse(5000, error.toString(), null)
        res.status(500).json(response)
      }
    }
  }
})

export default router
