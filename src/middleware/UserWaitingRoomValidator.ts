import { Request, Response, NextFunction } from 'express'
import WaitingRoomService from '../service/WaitingRoomService'
import IRequestAsUser from '../http/interface/IRequestWithUser'
import IRequestWithEvent from '../http/interface/IRequestWithEvent'
import IRequestWithWaitingRoom from '../http/interface/IRequestWithWaitingRoom'
import EventService from '../service/EventService'
import HTTPResponse from '../http/response/HTTPResponse'
import RequestException from '../exception/RequestException'
import { logger } from '../config/logger'

export default async (req: Request, res: Response, next: NextFunction) => {
  const reqWithUser = req as IRequestAsUser
  const { user } = reqWithUser
  const waitingRoomService = new WaitingRoomService()
  const waitingRoomId = req.params.waiting_room_id
  const waitingRoom = await waitingRoomService.findById(waitingRoomId)
  if (waitingRoom !== null && await waitingRoomService.isWaitingRoomOwnedByUser(waitingRoomId, user)) {
    const reqWithWaitingroom = req as IRequestWithWaitingRoom
    reqWithWaitingroom.waitingRoom = waitingRoom
    req = reqWithWaitingroom as Request
    next()
  } else {
    const response = new RequestException(4000, 'Waitingroom Not found', 404)
    res.status(response.httpStatusCode).json(new HTTPResponse(4000, response.message, null))
  }
}
