require('dotenv').config()

import express, { Request } from 'express'
import jwt from 'jsonwebtoken'
import HttpResponse from '../http/response/HTTPResponse'
import env from '../utility/ENV'
import UserService from '../service/UserService'
import UserJWTEntity from '../http/class/UserJWTEntity'
import IRequestWithUser from '../http/interface/IRequestWithUser'

const router = express.Router()

const excludeListRegex: Array<string> = [
  '^/$',
  '^/user/login/.*',
  '^/user/register/.*',
]

router.use('/', async (req, res, next) => {
  let exclude = false
  const reqWithUser = req as IRequestWithUser
  for (let i: number = 0; i < excludeListRegex.length; i += 1) {
    if (req.url.match(excludeListRegex[i])) {
      exclude = true
      break
    }
  }
  if (exclude) {
    next()
  } else {
    const token = req.headers.authorization
    if (token == null) {
      res.status(401).json(new HttpResponse(4000, 'No token provided', null))
    } else {
      try {
        const pureToken = token.substring(7)
        const userJson = JSON.stringify(await jwt.verify(pureToken, env.JWT_SCRET))
        const jwtEntity = UserJWTEntity.parseJSON(userJson)
        const userService = new UserService()
        const user = await userService.findById(jwtEntity.id)
        if (user == null) {
          throw new Error('User not found')
        }
        reqWithUser.user = user
        req = reqWithUser as Request
        next()
      } catch (err) {
        res.status(401).json(new HttpResponse(4000, err.toString(), null))
      }
    }
  }
})

export default router
