import { Request, Response, NextFunction } from 'express'
import WaitingRoomService from '../service/WaitingRoomService'
import IRequestAsUser from '../http/interface/IRequestWithUser'
import IRequestWithEvent from '../http/interface/IRequestWithEvent'
import IRequestWithWaitingRoom from '../http/interface/IRequestWithWaitingRoom'
import EventService from '../service/EventService'
import HTTPResponse from '../http/response/HTTPResponse'
import RequestException from '../exception/RequestException'
import { logger } from '../config/logger'

export default async (req: Request, res: Response, next: NextFunction) => {
  const { waitingRoom } = req as IRequestWithWaitingRoom

  const eventService = new EventService()
  const eventId = req.params.event_id
  const event = await eventService.findById(eventId)

  if (event !== null && event.waitingRoomId.toString() === waitingRoom.id.toString()) {
    const reqWithEvent = req as IRequestWithEvent
    reqWithEvent.event = event
    req = reqWithEvent as Request
    next()
  } else {
    const response = new RequestException(4000, 'Event Not found', 404)
    res.status(response.httpStatusCode).json(new HTTPResponse(4000, response.message, null))
  }
}
