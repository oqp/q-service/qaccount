require('dotenv').config()

import express from 'express'
import helmet from 'helmet'
import cors from 'cors'
import bodyParser from 'body-parser'
import { logger, httpLogger } from './config/logger'
import { mongoose } from './config/mongoose'
import zookeeper from './config/zookeeperClient'
import routes from './routes'

const PORT = process.env.PORT || 3000

const app = express()
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cors())
app.use(helmet())
if (process.env.LOG_LEVEL === 'info') {
  app.use(httpLogger)
}
app.use('/', routes)


app.get('/', (req, res) => {
  res.send(`Server is running! ${new Date()}`)
})

mongoose.connect(`mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`, { useNewUrlParser: true, connectTimeoutMS: 2000 })
  .catch((message) => {
    logger.error({ message })
    process.exitCode = 1
    process.exit()
  })
mongoose.connection.once('open', () => {
  logger.info('connected to mongodb')
  const zookeeperClient = zookeeper.getClient()
  logger.info('connecting to zookeeper')
  zookeeperClient.connect()
  zookeeperClient.on('connected', () => {
    logger.info('connected to zookeeper')
    app.listen(PORT, () => {
      logger.info(`Listening on ${PORT}`)
    })
  })
})
