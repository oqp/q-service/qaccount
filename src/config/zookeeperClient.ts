/* eslint @typescript-eslint/no-explicit-any: off */
require('dotenv').config()

import zookeeper from 'node-zookeeper-client'
import { logger } from './logger'

const client: zookeeper.Client = zookeeper.createClient(`${process.env.ZK_HOST}`)

const getClient = (): zookeeper.Client => client

const exist = (client: zookeeper.Client, path: string, watcher: any): Promise<any> => {
  return new Promise((resolve, reject): void => {
    if (client == null) {
      reject(new Error('No client specify'))
    } else {
      client.exists(path, watcher, (err, stat) => {
        if (err) reject(err)
        else resolve(stat)
      })
    }
  })
}

const getChildren = (client: zookeeper.Client, path: string, watcher: any): Promise<any> => {
  logger.debug('getting node\'s children')
  return new Promise((resolve, reject): void => {
    if (client == null) {
      reject(new Error('No client specify'))
    } else {
      client.getChildren(path, watcher, (err, children) => {
        if (err) reject(err)
        else {
          resolve(children)
        }
      })
    }
  })
}

const create = (client: zookeeper.Client, path: string, data: any, acl: any, mode: any): Promise<any> => {
  logger.debug('creating node')
  return new Promise((resolve, reject): void => {
    if (client == null) {
      reject(new Error('No client specify'))
    } else {
      client.create(path, data, acl, mode, (err, path: string) => {
        if (err) reject(err)
        else resolve(path)
      })
    }
  })
}

const remove = (client: zookeeper.Client, path: string, version: any): Promise<any> => {
  logger.debug('removing node')
  return new Promise((resolve, reject): void => {
    if (client == null) {
      reject(new Error('No client specify'))
    } else {
      if (version) {
        client.remove(path, version, (err) => {
          if (err) reject(err)
          else resolve()
        })
      } else {
        client.remove(path, (err) => {
          if (err) reject(err)
          else resolve()
        })
      }
    }
  })
}

const getData = (client: zookeeper.Client, path: string, watcher: any): Promise<any> => {
  logger.debug('getting node\'s data')
  return new Promise((resolve, reject): void => {
    if (client == null) {
      reject(new Error('No client specify'))
    } else {
      client.getData(path, watcher, (err, data) => {
        if (err == null) {
          resolve(data)
        } else {
          reject(err)
        }
      })
    }
  })
}

const setData = (client: zookeeper.Client, path: string, data: any): Promise<any> => {
  logger.debug('setting node\'s data')
  return new Promise((resolve, reject): void => {
    if (client == null) {
      reject(new Error('No client specify'))
    } else {
      client.setData(path, data, (err, data) => {
        if (err == null) {
          resolve(data)
        } else {
          reject(err)
        }
      })
    }
  })
}

export default {
  getClient,
  exist,
  getChildren,
  create,
  remove,
  getData,
  setData,
}
