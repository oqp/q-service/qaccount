import express from 'express'
import { UserModel } from './model/User'
import { getStatus } from './config/mongoose'
import ServiceStatusResponse from './http/response/ServiceStatusResponse'
import jwtDecoderMiddleware from './middleware/JWTDecoder'
import userController from './controller/UserController'
import waitingRoomController from './controller/WaitingRoomController'

const router = express.Router()
router.use(jwtDecoderMiddleware)

router.use('/user', userController)
router.use('/waitingRoom', waitingRoomController)

router.get('/test', async (req, res) => {
  UserModel.create({ firstname: 'hi' })
  res.send(await UserModel.find())
})

router.get('/status', async (req, res) => {
  const mongodbStatus = getStatus()
  const response: Record<string, ServiceStatusResponse> = {
    mongodb: mongodbStatus,
  }
  let responseStatusCode: number = 200
  Object.keys(response).forEach((key: string) => {
    if (response[key].isOK === false) {
      responseStatusCode = 500
    }
  })
  res.statusCode = responseStatusCode
  res.send(response)
})

export = router
