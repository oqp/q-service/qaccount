import RequestException from './RequestException'

export default class DuplicateSubdomain extends RequestException {
  constructor() {
    super(4020, 'DuplicateSubdomain', 400)
  }
}
