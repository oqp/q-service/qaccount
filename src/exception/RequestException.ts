/* eslint @typescript-eslint/no-explicit-any: off */
import HTTPResponse from '../http/response/HTTPResponse'

export default class RequestException extends Error {
  code: number

  httpStatusCode: number

  constructor(code: number = 5000, message: string, httpStatusCode?: number) {
    super(message)
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, RequestException)
    }

    this.name = 'RequestException'
    this.code = code
    if (httpStatusCode) {
      this.httpStatusCode = httpStatusCode
    } else if (code >= 2000 && code < 4000) {
      this.httpStatusCode = 200
    } else if (code >= 4000 && code < 5000) {
      this.httpStatusCode = 400
    } else {
      this.httpStatusCode = 500
    }
  }

  getHTTPResponse(): HTTPResponse {
    return new HTTPResponse(this.code, this.message, null)
  }

  toString(): string {
    return this.message
  }
}
