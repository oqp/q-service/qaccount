import RequestException from './RequestException'

export default class InvalidSubdomainForm extends RequestException {
  constructor() {
    super(4021, 'InvalidSubdomainForm', 400)
  }
}
