import bcrypt from 'bcrypt'

const hashRound: number = 10

export default class Hash {
  static async hash(password: string): Promise<string> {
    return bcrypt.hash(password, hashRound)
  }

  static async isHashMatch(plainPassword: string, hashedPassword: string): Promise<boolean> {
    return bcrypt.compare(plainPassword, hashedPassword)
  }
}
