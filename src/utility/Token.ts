import crypto from 'crypto'

export default class Token {
  public static generateRandom(length: number): string {
    return crypto.randomBytes(length).toString('hex')
  }
}
