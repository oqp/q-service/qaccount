import uuidV4 from 'uuid/v4'
import uuidV5 from 'uuid/v5'

export default (name: string, namespace?: string): string => {
  if (namespace) {
    return uuidV5(name, namespace)
  }
  return uuidV5(name, uuidV4())
}
