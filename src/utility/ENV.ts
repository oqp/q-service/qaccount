export default class ENV {
  static get JWT_SCRET(): string {
    return process.env.JWT_SECRET == null ? '' : process.env.JWT_SECRET
  }
}
