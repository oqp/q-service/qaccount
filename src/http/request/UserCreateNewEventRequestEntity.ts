import BaseRequestEntity from './BaseRequestEntity'

export default class UserLoginByEmailRequestEntity extends BaseRequestEntity {
  name: string = '';

  redirectURL: string = '';

  maxOutflowAmount: number = 10;

  sessionTime: number = 600;

  arrivalTime: number = 300;

  openTime: Date = new Date();

  closeTime: Date = new Date();

  isActive: boolean = false;

  imageURL: string = '';

  subdomain: string = '';

}
