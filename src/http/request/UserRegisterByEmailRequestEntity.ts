import BaseRequestEntity from './BaseRequestEntity'

export default class UserRegisterByEmailRequestEntity extends BaseRequestEntity {
  email: string = '';

  password: string = '';

  confirmPassword: string = '';

  firstname: string = '';

  lastname: string = '';

  telno: string = '';

  acceptTerm: boolean = false;
}
