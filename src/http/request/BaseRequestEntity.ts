import moment from 'moment'

export default class BaseRequestEntity implements Record<string, any> {
  parseFromBodyRequest(bodyRequest: Record<string, any>): void {
    const modelKey = Object.keys(this)
    modelKey.forEach((key: string) => {
      const updateKey: Record<string, any> = {}
      if (updateKey[key] instanceof Date) {
        updateKey[key] = moment(bodyRequest[key]).toDate()
      } else {
        updateKey[key] = bodyRequest[key]
      }
      Object.assign(this, updateKey)
    })
  }
}
