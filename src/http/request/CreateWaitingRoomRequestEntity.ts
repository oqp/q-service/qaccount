import BaseRequestEntity from './BaseRequestEntity'

export default class CreateWaitingRoomRequestEntity extends BaseRequestEntity {
  name: string = '';

  website: string = '';

  description: string = '';
}
