import BaseRequestEntity from './BaseRequestEntity'

export default class UserLoginByEmailRequestEntity extends BaseRequestEntity {
  email: string = '';

  password: string = '';
}
