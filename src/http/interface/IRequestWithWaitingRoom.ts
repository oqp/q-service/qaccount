import { Request } from 'express'
import WaitingRoom from '../../model/WaitingRoom'

export default interface IRequestWithWaitingRoom extends Request {
  waitingRoom: WaitingRoom;
}
