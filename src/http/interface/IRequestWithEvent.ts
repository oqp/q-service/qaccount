import { Request } from 'express'
import Event from '../../model/Event'

export default interface IRequestWithEvent extends Request {
  event: Event;
}
