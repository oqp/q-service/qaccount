import { Request } from 'express'
import User from '../../model/User'

export default interface IRequestWithUser extends Request {
  user: User;
}
