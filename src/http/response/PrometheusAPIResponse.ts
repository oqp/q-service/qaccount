export default interface IPrometheusAPIResponse {
  status: string;

  data: IResultData;
}

export interface IResultData {
  resultType: string;

  result: Array<IMetricData>;
}

export interface IMetricData {
  metric: Map<string, string>;

  value: Array<Array<number | string> | number | string>;
}
