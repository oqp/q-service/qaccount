import { Schema, SchemaTypes, Document } from 'mongoose'

import { mongoose } from '../config/mongoose'
import BaseModel from './BaseModel'

interface IUserWaitingRoom {
  id: string;
  waitingRoomId: string;
  userId: string;
}

export interface IUserWaitingRoomModel extends Document, IUserWaitingRoom {
  id: string;
  waitingRoomId: string;
  userId: string;
}

export const userWaitingRoomSchema = new Schema({
  id: SchemaTypes.ObjectId,
  waitingRoomId: SchemaTypes.ObjectId,
  userId: SchemaTypes.ObjectId,
})

export const UserWaitingRoomModel = mongoose.model<IUserWaitingRoomModel>('UserWaitingRoom', userWaitingRoomSchema)

export default class UserWaitingRoom extends BaseModel implements IUserWaitingRoom {
  id: string;

  waitingRoomId: string = '';

  userId: string = '';
}
