import { Schema, SchemaTypes, Document } from 'mongoose'
import { mongoose } from '../config/mongoose'
import BaseModel from './BaseModel'

interface IWaitingRoom {
  id: string;
  uuid: string;
  name: string;
  website: string;
  description: string;
}

export interface IWaitingRoomModel extends Document, IWaitingRoom {
  id: string;
  uuid: string;
  name: string;
  website: string;
  description: string;
}

export const waitingRoomSchema = new Schema({
  id: SchemaTypes.ObjectId,
  uuid: String,
  name: String,
  website: String,
  description: String,
})

export const WaitingRoomModel = mongoose.model<IWaitingRoomModel>('WaitingRoom', waitingRoomSchema)

export default class WaitingRoom extends BaseModel implements IWaitingRoom {
  id: string;

  uuid: string = '';

  name: string = '';

  website: string = '';

  description: string = '';
}
