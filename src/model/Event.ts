import { Schema, SchemaTypes, Document } from 'mongoose'
import { mongoose } from '../config/mongoose'
import BaseModel from './BaseModel'

interface IEvent {
  id: string;
  uuid: string;
  name: string;
  openTime: Date;
  closeTime: Date;
  maxOutflowAmount: number;
  currentQueueNumber: number;
  sessionTime: number;
  arrivalTime: number;
  redirectURL: string;
  isActive: boolean;
  isScheduled: boolean;
  integrateToken: string;
  waitingRoomId: string;
  imageURL: string;
  subdomain: string;
}

export interface IEventModel extends Document, IEvent {
  id: string;
  uuid: string;
  name: string;
  openTime: Date;
  closeTime: Date;
  maxOutflowAmount: number;
  currentQueueNumber: number;
  sessionTime: number;
  arrivalTime: number;
  redirectURL: string;
  isActive: boolean;
  isScheduled: boolean;
  integrateToken: string;
  waitingRoomId: string;
  imageURL: string;
  subdomain: string;
}

export const eventSchema = new Schema({
  id: SchemaTypes.ObjectId,
  uuid: String,
  name: String,
  openTime: Date,
  closeTime: Date,
  maxOutflowAmount: Number,
  currentQueueNumber: Number,
  sessionTime: Number,
  arrivalTime: Number,
  redirectURL: String,
  isActive: Boolean,
  isScheduled: Boolean,
  integrateToken: String,
  waitingRoomId: SchemaTypes.ObjectId,
  imageURL: String,
  subdomain: String,
})

export const EventModel = mongoose.model<IEventModel>('Event', eventSchema)

export default class Event extends BaseModel implements IEvent {
  id: string;

  uuid: string = '';

  name: string = '';

  openTime: Date = new Date();

  closeTime: Date = new Date();

  maxOutflowAmount: number = 0;

  currentQueueNumber: number = 0;

  sessionTime: number = 600;

  arrivalTime: number = 300;

  redirectURL: string = '';

  isActive: boolean = false;

  isScheduled: boolean = false;

  waitingRoomId: string = '';

  integrateToken: string = '';

  imageURL: string = '';

  subdomain: string = '';

  toJSON(): object {
    const tempObject = { ...this }
    if (tempObject.integrateToken) {
      tempObject.integrateToken = 'generated'
    }
    return tempObject
  }
}
