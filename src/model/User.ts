import { Schema, SchemaTypes, Document } from 'mongoose'

import { mongoose } from '../config/mongoose'
import BaseModel from './BaseModel'

interface UserInterface {
  id: string;
  firstname: string;
  lastname: string;
  password: string;
  telno: string;
  email: string;
}

export interface UserModelInterface extends Document, UserInterface {
  id: string;
  firstname: string;
  lastname: string;
  password: string;
  telno: string;
  email: string;
}

export const userSchema = new Schema({
  id: SchemaTypes.ObjectId,
  firstname: String,
  lastname: String,
  password: String,
  telno: String,
  email: String,
})

export const UserModel = mongoose.model<UserModelInterface>('User', userSchema)

export default class User extends BaseModel implements UserInterface {
  id: string;

  firstname: string = '';

  lastname: string = '';

  password: string = '';

  telno: string = '';

  email: string = '';

  toJSON(): object {
    const tempObject = { ...this }
    delete tempObject.password
    return tempObject
  }
}
