require('dotenv').config()

import jwt from 'jsonwebtoken'
import UserRepository from '../repository/UserRepository'
import UserRepositoryInterface from '../repository/interface/IUserRepository'
import User from '../model/User'
import UserRegisterByEmailRequestEntity from '../http/request/UserRegisterByEmailRequestEntity'
import Hash from '../utility/Hash'
import UserLoginByEmailRequestEntity from '../http/request/UserLoginByEmailRequestEntity'
import RequestException from '../exception/RequestException'

export default class UserService {
  userRepository: UserRepositoryInterface;

  constructor() {
    this.userRepository = new UserRepository()
  }

  async loginByEmail(request: UserLoginByEmailRequestEntity): Promise<string> {
    const user: User | null = await this.userRepository.findByEmail(request.email)
    if (user === null) {
      throw new RequestException(4000, 'Email or Hash not correct', 401)
    }
    const isPasswordMatch: boolean = await Hash.isHashMatch(request.password, user.password)
    if (isPasswordMatch === false) {
      throw new RequestException(4000, 'Email or Hash not correct', 401)
    }
    const token = jwt.sign(
      { id: user.id },
      `${process.env.JWT_SECRET}`,
      { expiresIn: 60 * 15 },
    )
    return new Promise<string>((resolve): void => resolve(token))
  }

  async refreshToken(user: User): Promise<string> {
    const token = jwt.sign(
      { id: user.id },
      `${process.env.JWT_SECRET}`,
      { expiresIn: 60 * 15 },
    )
    return new Promise<string>((resolve): void => resolve(token))
  }

  async registerByEmail(request: UserRegisterByEmailRequestEntity): Promise<User> {
    if (request.password !== request.confirmPassword) {
      throw new RequestException(4004, 'Password mismatch')
    }
    const checkEmail: User | null = await this.userRepository.findByEmail(request.email)
    if (checkEmail !== null) {
      throw new RequestException(4003, 'Duplicate Email')
    }
    if (request.acceptTerm !== true) {
      throw new RequestException(4005, 'Not accept term')
    }
    const user = new User()
    user.email = request.email
    user.firstname = request.firstname
    user.lastname = request.lastname
    user.telno = request.telno
    user.password = await Hash.hash(request.password)
    const userSaved = await this.userRepository.save(user)
    return userSaved
  }

  async findById(id: string): Promise<User | null> {
    const user: User | null = await this.userRepository.findById(id)
    return user
  }
}
