import User from '../model/User'
import WaitingRoomRepository from '../repository/WaitingRoomRepository'
import UserWaitingRoomRepository from '../repository/UserWaitingRoomRepository'
import CreateWaitingRoomRequestEntity from '../http/request/CreateWaitingRoomRequestEntity'
import UserWaitingRoom from '../model/UserWaitingRoom'
import WaitingRoom from '../model/WaitingRoom'
import RequestException from '../exception/RequestException'
import UUID from '../utility/UUID'

export default class WaitingRoomService {
  waitingRoomRepository: WaitingRoomRepository;

  userWaitingRoomRepository: UserWaitingRoomRepository;

  constructor() {
    this.waitingRoomRepository = new WaitingRoomRepository()
    this.userWaitingRoomRepository = new UserWaitingRoomRepository()
  }

  async createWaitingRoom(user: User, request: CreateWaitingRoomRequestEntity):
  Promise<WaitingRoom> {
    const waitingRoom = new WaitingRoom()
    waitingRoom.name = request.name
    waitingRoom.website = request.website
    waitingRoom.uuid = UUID('waitingRoom')
    waitingRoom.description = request.description
    const createdWaitingRoom = await this.waitingRoomRepository.save(waitingRoom)
    const userWaitingRoom = new UserWaitingRoom()
    userWaitingRoom.userId = user.id
    userWaitingRoom.waitingRoomId = createdWaitingRoom.id
    await this.userWaitingRoomRepository.save(userWaitingRoom)
    return createdWaitingRoom
  }

  async findWaitingRoomByUser(user: User): Promise<WaitingRoom[]> {
    const waitingRoomIdList = await this.userWaitingRoomRepository.findByUserId(user.id)
    const waitingRooms = new Array<WaitingRoom>()
    for (let i: number = 0; i < waitingRoomIdList.length; i += 1) {
      const { waitingRoomId } = waitingRoomIdList[i]
      const waitingRoom = await this.waitingRoomRepository.findById(waitingRoomId)
      if (waitingRoom !== null) {
        waitingRooms.push(waitingRoom)
      }
    }
    return waitingRooms
  }

  async isWaitingRoomOwnedByUser(waitingRoomId: string, user: User): Promise<boolean> {
    const waitingRoomIdList = await this.userWaitingRoomRepository.findByUserId(user.id)
    for (let i: number = 0; i < waitingRoomIdList.length; i += 1) {
      if (waitingRoomIdList[i].waitingRoomId.toString() === waitingRoomId) {
        return true
      }
    }
    return false
  }

  async findById(waitingRoomId: string): Promise<WaitingRoom | null> {
    const waitingRoom = await this.waitingRoomRepository.findById(waitingRoomId)
    return waitingRoom
  }

  async editWaitingRoom(waitingRoomId: string, waitingRoomRequest: CreateWaitingRoomRequestEntity): Promise<WaitingRoom> {
    const waitingRoom = await this.waitingRoomRepository.findById(waitingRoomId)
    const waitingRoomNotFoundException = new RequestException(4000, 'Waiting Room Not found', 400)
    if (waitingRoom === null) {
      throw waitingRoomNotFoundException
    }
    waitingRoom.name = waitingRoomRequest.name
    waitingRoom.website = waitingRoomRequest.website
    waitingRoom.description = waitingRoomRequest.description
    const newWaitingRoom = await this.waitingRoomRepository.update(waitingRoomId, waitingRoom)
    if (newWaitingRoom === null) {
      throw waitingRoomNotFoundException
    }
    return newWaitingRoom
  }

  async deleteWaitingRoom(waitingRoom: WaitingRoom) {
    const notFoundException = new RequestException(4000, 'Waiting Room not found', 404)
    const deletedWaitingRoom = this.waitingRoomRepository.delete(waitingRoom)
    if (deletedWaitingRoom === null) {
      throw notFoundException
    }
    return waitingRoom
  }
}
