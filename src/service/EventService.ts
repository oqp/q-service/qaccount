require('dotenv').config()

import zookeeper from 'node-zookeeper-client'
import DuplicateSubdomain from '../exception/DuplicateSubdomain'
import InvalidSubdomainForm from '../exception/InvalidSubdomainForm'
import EventRepository from '../repository/EventRepository'
import WaitingRoom from '../model/WaitingRoom'
import Event from '../model/Event'
import UserCreateNewEventRequestEntity from '../http/request/UserCreateNewEventRequestEntity'
import Token from '../utility/Token'
import Hash from '../utility/Hash'
import UUID from '../utility/UUID'
import RequestException from '../exception/RequestException'
import zookeeperClient from '../config/zookeeperClient'
import { logger } from '../config/logger'

export default class EventService {
  eventRepository: EventRepository

  zookeeperClient: zookeeper.Client

  constructor() {
    this.eventRepository = new EventRepository()
    this.zookeeperClient = zookeeperClient.getClient()
  }


  async validateSubdomain(event: Event): Promise<Event> {
    const regex = /^[a-zA-Z0-9-]+$/
    if (regex.test(event.subdomain) === false) {
      throw new InvalidSubdomainForm()
    }
    const searchedEvent = await this.eventRepository.findOne({ subdomain: event.subdomain })
    if (searchedEvent !== null) {
      throw new DuplicateSubdomain()
    }
    return event
  }

  async addEvent(request: UserCreateNewEventRequestEntity, waitingRoom: WaitingRoom):
  Promise<Event> {
    const event = new Event()
    event.name = request.name
    event.currentQueueNumber = 0
    event.closeTime = request.closeTime
    event.isActive = false
    event.maxOutflowAmount = request.maxOutflowAmount
    event.openTime = request.openTime
    event.redirectURL = request.redirectURL
    event.uuid = UUID('event')
    event.waitingRoomId = waitingRoom.id
    event.imageURL = request.imageURL
    event.subdomain = request.subdomain
    event.arrivalTime = request.arrivalTime
    await this.validateSubdomain(event)
    const returnEvent = await this.eventRepository.save(event)
    return returnEvent
  }

  async findByWaitingRoomId(waitingRoomId: string): Promise<Event[]> {
    const events = await this.eventRepository.findByWaitingRoomId(waitingRoomId)
    return events
  }

  async findById(eventId: string): Promise<Event | null> {
    const event = await this.eventRepository.findById(eventId)
    return event
  }

  async generateIntegrateToken(eventId: string): Promise<string> {
    const token = Token.generateRandom(20)
    const hashedToken = Hash.hash(token)
    const event = await this.eventRepository.findById(eventId)
    if (event == null) {
      throw new RequestException(404, 'Event not found', undefined)
    } else {
      event.integrateToken = await hashedToken
      await this.eventRepository.update(event)
      return token
    }
  }

  async setEventIsActiveStatus(eventId: string, isActive: boolean): Promise<Event | null> {
    logger.info('setting event status', {
      eventId,
    })
    try {
      const event = await this.eventRepository.findById(eventId)
      if (event !== null) {
        if (event.isActive === isActive) {
          throw new RequestException(4000, `event is already ${isActive ? 'active' : 'inactive'}`, 400)
        }
        if (isActive === false) {
          event.isActive = false
        } else if (isActive === true) {
          event.isActive = true
          if (event.isScheduled === false) {
            event.isScheduled = true
            await this.addToZookeeperProcessingNode(event.uuid)
          }
        }
        const updatedEvent = await this.eventRepository.update(event)
        return updatedEvent
      }
    } catch (error) {
      logger.error(error.toString())
      throw error
    }
    return null
  }

  async addToZookeeperProcessingNode(eventUUID: string): Promise<boolean> {
    logger.info('adding event to processing node', {
      eventUUID,
    })
    try {
      await zookeeperClient.create(
        zookeeperClient.getClient(),
        `${process.env.ZK_QUEUE_BASE_ONLINE_PATH}/${new Date().getTime()}-event-${eventUUID}`,
        Buffer.from(
          JSON.stringify({
            event_uuid: eventUUID,
          }),
          'utf-8',
        ),
        null,
        null,
      )
      return true
    } catch (error) {
      logger.error('failed to create node', {
        eventUUID,
        error,
      })
      throw error
    }
  }

  async removeFromZookeeperProcessingNode(eventUUID: string): Promise<boolean> {
    const BASE_PATH = process.env.ZK_QUEUE_BASE_PATH
    const BASE_ONLINE_PATH = process.env.ZK_QUEUE_BASE_ONLINE_PATH
    const BASE_PROCESSING_PATH = process.env.ZK_QUEUE_BASE_PROCESSING_PATH
    if (BASE_PATH && BASE_ONLINE_PATH && BASE_PROCESSING_PATH) {
      logger.info(`finding ${eventUUID} node in ${BASE_ONLINE_PATH}`)
      const onlineNodes: [] = await zookeeperClient.getChildren(
        zookeeperClient.getClient(),
        BASE_ONLINE_PATH,
        null,
      )
      const targetOnlineNode = onlineNodes.find((node: string) => {
        return node.includes(eventUUID)
      })
      if (targetOnlineNode) {
        try {
          await zookeeperClient.remove(zookeeperClient.getClient(), `${BASE_ONLINE_PATH}/${targetOnlineNode}`, null)
        } catch (error) {
          logger.error(error)
          throw error
        }
      } else {
        logger.info(`finding ${eventUUID} node in ${BASE_PROCESSING_PATH}`)
        const processingNodes: [] = await zookeeperClient.getChildren(
          zookeeperClient.getClient(),
          BASE_PROCESSING_PATH,
          null,
        )
        const targetProcessingNode = processingNodes.find((node: string) => {
          return node.includes(eventUUID)
        })
        if (targetProcessingNode) {
          try {
            await zookeeperClient.remove(zookeeperClient.getClient(), `${BASE_PROCESSING_PATH}/${targetOnlineNode}`, null)
          } catch (error) {
            logger.error(error)
            throw error
          }
        } else {
          const errorMessage = `${eventUUID} node not found`
          throw new Error(errorMessage)
        }
      }
      logger.info('removed node', {
        eventUUID,
      })
      return true
    }
    throw new Error('base online path or processing path is undefined')
  }

  async editEvent(eventId: string, eventRequest: UserCreateNewEventRequestEntity): Promise<Event | null> {
    const eventNotFoundException = new RequestException(4000, 'Event not found', 404)
    const event = await this.eventRepository.findById(eventId)
    if (event === null) {
      throw eventNotFoundException
    }
    const incomingSubdomain = String(eventRequest.subdomain)
    const currentSubdomain = String(event.subdomain)
    event.name = eventRequest.name
    if (eventRequest.imageURL) {
      event.imageURL = eventRequest.imageURL
    }
    event.sessionTime = eventRequest.sessionTime
    event.openTime = eventRequest.openTime
    event.closeTime = eventRequest.closeTime
    event.arrivalTime = eventRequest.arrivalTime
    event.maxOutflowAmount = eventRequest.maxOutflowAmount
    event.isActive = eventRequest.isActive
    event.redirectURL = eventRequest.redirectURL
    event.subdomain = eventRequest.subdomain
    try {
      await this.validateSubdomain(event)
    } catch (error) {
      if (incomingSubdomain !== currentSubdomain) {
        throw error
      }
    }
    const updatedEvent = await this.eventRepository.update(event)
    return updatedEvent
  }

  async deleteEvent(event: Event) {
    const eventNotFoundException = new RequestException(4000, 'Event not found', 404)
    if (event.isActive) {
      await this.setEventIsActiveStatus(event.id, false)
    }
    const deletedEvent = this.eventRepository.delete(event)
    if (deletedEvent === null) {
      throw eventNotFoundException
    }
    return deletedEvent
  }
}
