require('dotenv').config()

import axios, { AxiosInstance } from 'axios'
import FormData from 'form-data'
import Event from '../model/Event'
import IPrometheusAPIResponse from '../http/response/PrometheusAPIResponse'
import { logger } from '../config/logger'

export default class PrometheusService {
  client: AxiosInstance;

  constructor() {
    this.client = axios.create({
      baseURL: `${process.env.PROMETHEUS_URL}/api/v1`,
      timeout: 5000,
      transformResponse: (response: string): IPrometheusAPIResponse => {
        const myObj: IPrometheusAPIResponse = JSON.parse(response)
        return myObj
      },
    })
  }

  async getCurrentQueueNumber(event: Event): Promise<number | null> {
    try {
      const formData = new FormData()
      formData.append('query', `oqp_event_current_queue_number{eventUUID="${event.uuid}"}`)
      const response = await this.client.post<IPrometheusAPIResponse>('/query', formData.getBuffer(), {
        headers: formData.getHeaders(),
      })
      logger.debug(typeof response.data)
      if (typeof response.data.data.result[0].value[1] === 'string') {
        return parseInt(response.data.data.result[0].value[1], 10)
      }
      return null
    } catch (err) {
      logger.debug(err.toString())
      return null
    }
  }

  async getMaxSessionTime(event: Event): Promise<number | null> {
    const formData = new FormData()
    formData.append('query', `oqp_event_max_session_time{eventUUID="${event.uuid}"}`)
    const response = await this.client.post<IPrometheusAPIResponse>('/query', formData.getBuffer(), {
      headers: formData.getHeaders(),
    })
    try {
      if (typeof response.data.data.result[0].value[1] === 'string') {
        return parseFloat((parseInt(response.data.data.result[0].value[1], 10) / 1000).toFixed(2))
      }
      return null
    } catch (err) {
      return null
    }
  }

  async getMinSessionTime(event: Event): Promise<number | null> {
    const formData = new FormData()
    formData.append('query', `oqp_event_min_session_time{eventUUID="${event.uuid}"}`)
    const response = await this.client.post<IPrometheusAPIResponse>('/query', formData.getBuffer(), {
      headers: formData.getHeaders(),
    })
    try {
      if (typeof response.data.data.result[0].value[1] === 'string') {
        return parseFloat((parseInt(response.data.data.result[0].value[1], 10) / 1000).toFixed(2))
      }
      return null
    } catch (err) {
      return null
    }
  }

  async getAvgSessionTime(event: Event): Promise<number | null> {
    const formData = new FormData()
    formData.append('query', `oqp_event_avg_session_time{eventUUID="${event.uuid}"}`)
    const response = await this.client.post<IPrometheusAPIResponse>('/query', formData.getBuffer(), {
      headers: formData.getHeaders(),
    })
    try {
      if (typeof response.data.data.result[0].value[1] === 'string') {
        return parseFloat((parseInt(response.data.data.result[0].value[1], 10) / 1000).toFixed(2))
      }
      return null
    } catch (err) {
      return null
    }
  }

  async getCompletedAudience(event: Event): Promise<number | null> {
    const formData = new FormData()
    formData.append('query', `oqp_event_completed_audience{eventUUID="${event.uuid}"}`)
    const response = await this.client.post<IPrometheusAPIResponse>('/query', formData.getBuffer(), {
      headers: formData.getHeaders(),
    })
    try {
      if (typeof response.data.data.result[0].value[1] === 'string') {
        return parseInt(response.data.data.result[0].value[1], 10)
      }
      return null
    } catch (err) {
      return null
    }
  }

  async getSessionTimedoutAudience(event: Event): Promise<number | null> {
    const formData = new FormData()
    formData.append('query', `oqp_event_session_timedout_audience{eventUUID="${event.uuid}"}`)
    const response = await this.client.post<IPrometheusAPIResponse>('/query', formData.getBuffer(), {
      headers: formData.getHeaders(),
    })
    try {
      if (typeof response.data.data.result[0].value[1] === 'string') {
        return parseInt(response.data.data.result[0].value[1], 10)
      }
      return null
    } catch (err) {
      return null
    }
  }

  async getWaitingAudience(event: Event): Promise<number | null> {
    const formData = new FormData()
    formData.append('query', `oqp_event_waiting_audience{eventUUID="${event.uuid}"}`)
    const response = await this.client.post<IPrometheusAPIResponse>('/query', formData.getBuffer(), {
      headers: formData.getHeaders(),
    })
    try {
      if (typeof response.data.data.result[0].value[1] === 'string') {
        return parseInt(response.data.data.result[0].value[1], 10)
      }
      return null
    } catch (err) {
      return null
    }
  }

  async getArrivalTimedoutAudience(event: Event): Promise<number | null> {
    const formData = new FormData()
    formData.append('query', `oqp_event_arrival_timedout_audience{eventUUID="${event.uuid}"}`)
    const response = await this.client.post<IPrometheusAPIResponse>('/query', formData.getBuffer(), {
      headers: formData.getHeaders(),
    })
    try {
      if (typeof response.data.data.result[0].value[1] === 'string') {
        return parseInt(response.data.data.result[0].value[1], 10)
      }
      return null
    } catch (err) {
      return null
    }
  }

  async getServingAudience(event: Event): Promise<number | null> {
    const formData = new FormData()
    formData.append('query', `oqp_event_serving_audience_audience{eventUUID="${event.uuid}"}`)
    const response = await this.client.post<IPrometheusAPIResponse>('/query', formData.getBuffer(), {
      headers: formData.getHeaders(),
    })
    try {
      if (typeof response.data.data.result[0].value[1] === 'string') {
        return parseInt(response.data.data.result[0].value[1], 10)
      }
      return null
    } catch (err) {
      return null
    }
  }
}
