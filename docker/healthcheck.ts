/* eslint no-console: off */

require('dotenv').config()

import http = require('http')

const options = {
  hostname: 'localhost',
  port: process.env.PORT,
  timeout: 2000,
}

const request: http.ClientRequest = http.request(options, (res) => {
  console.log(`STATUS: ${res.statusCode}`)
  if (res.statusCode === 200) {
    process.exit(0)
  } else {
    process.exit(1)
  }
})

request.on('error', (err) => {
  console.log('ERROR', err)
  process.exit(1)
})

request.end()
